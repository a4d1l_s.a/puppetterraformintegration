provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "ec2_example" {

    ami           = "ami-07d9b9ddc6cd8dd30"
    instance_type = "t2.micro"
    key_name = "terr"
    vpc_security_group_ids = [aws_security_group.main.id]
    

  connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
     
      private_key = file("C:/Users/aadil/.ssh/id_rsa")
      timeout     = "4m"
   }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet",
      
    ]
  }

  provisioner "file" {
    source      = "C:/assignment4/puppet"
    destination = "/tmp/puppets"
  }

    provisioner "remote-exec" {
    inline = [
      "sudo /opt/puppetlabs/bin/puppet apply /tmp/puppet/manifest.pp"
      
    ]
  }
}

resource "aws_security_group" "main" {
  egress = [
    {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]
 ingress                = [
   {
     cidr_blocks      = [ "0.0.0.0/0", ]
     description      = ""
     from_port        = 22
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     protocol         = "tcp"
     security_groups  = []
     self             = false
     to_port          = 22
  }
  ]
}

