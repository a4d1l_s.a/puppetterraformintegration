package { 'nginx':
  ensure => installed,
}

s
service { 'nginx':
  ensure  => running,
  enable  => true,
}