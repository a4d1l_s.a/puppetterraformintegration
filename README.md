Terraform folder contains the terraform file.
similiarly the puppet contains the puppet manifest.

1. Setup GitLab Repository:
Created a new GitLab repository named "PuppetTerraformIntegration".
 

2. Infrastructure Provisioning with Terraform:
A terraform code was written to provision a simple Amazon EC2 instance
 
 
3. Configuration Management with Puppet:

Wrote a Puppet manifests to configure the provisioned infrastructure component.
 
This particular manifest would install nginx and make sure it is running.
4. Integration:

To make sure Puppet configuration management is automatically triggered after Terraform provisioning.
I have used terraform provisioning.
To make sure provisioning works Firstly, I had to set up a connection to the instance
 
Then I have used remote-exec provisioner to update and then install puppet
 
Then I have  used file provisioning to transfer my puppet manifest to the instance
 
Similarly now we are going to use remote-exec to apply the puppet manifest
